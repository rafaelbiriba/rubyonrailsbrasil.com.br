# frozen_string_literal: true

source 'https://rubygems.org'

gem 'rake', '~> 12.3'

# Middleman Gems
gem 'middleman', '~> 4.3.0'
gem 'middleman-blog', '~> 4.0'
gem 'middleman-disqus', '~> 1.2.0'
gem 'middleman-google-analytics', '~> 3.0'
gem 'middleman-livereload'
gem 'middleman-sprockets', '~> 4.1.0'

# For feed.xml.builder
gem 'builder', '~> 3.2.2'

# Code syntax highlighting
gem 'middleman-syntax', '~> 3.0'
gem 'redcarpet', '~> 3.3.4'

# Asset pipeline
gem 'mini_racer'
gem 'nokogiri', '~> 1.10'
gem 'sass'
gem 'sprockets', '~> 3.7.0'

# Codestyle
gem 'rubocop', '~> 0.76.0'

# External Assets
source 'https://rails-assets.org' do
  gem 'rails-assets-chartjs', '~> 2.6'
  gem 'rails-assets-jquery'
end
