---
title: "Ambiente de Desenvolvimento: Instalando bancos de dados"
date: 2020-05-16 22:00 UTC
tags: iniciante
layout: post
author: brodock
---

Esse artigo é uma continuação do [anterior](/2020/03/01/iniciante-ambiente-de-desenvolvimento-ruby/), onde expliquei
como instalar o Ruby na sua máquina, nas diversas versões que você quiser.

Como no artigo anterior, vamos focar os exemplos aqui em Linux, distribuição Ubuntu, por que é a mais popular e, 
portanto, a que existe uma maior chance de você conseguir ajuda ou encontrar respostas sozinho na internet.

Se você usa outra distribuição ou deseja aprender outra, procure alguém que já a utilize pra te ajudar com dúvidas
ou te mostrar o caminho quando precisar. Na dúvida, use Ubuntu.

Entenda também que os exemplos aqui são direcionados a quem está começando a aprender Ruby on Rails. São exemplos
simplificados e não recomendável para utilizar em produção (isso vai ser abordado num artigo futuro).

Vamos falar aqui sobre bancos de dados focando em como você pode instalar e rodar os três principais bancos de dados
utilizados em uma aplicação Rails (e algumas variantes deles):

* MySQL (ou MariaDB)
* PostgreSQL
* Redis

Os dois primeiros são bancos SQL relacionais, e são os mais comuns utilizados como base de dados principal de uma
aplicação. É onde você vai armazenar desde as credenciais de usuário, para posteriormente autenticar eles no sistema,
a maioria dos dados gerados pelo usuário (com exceção de arquivos, vamos falar sobre arquivos num artigo futuro).

Redis é um banco chave-valor bastante flexível, cobrindo várias funcionalidades de uma única vez.
Os dois usos mais comuns são: _cache_ e _background jobs_. Vamos ver com mais detalhes a seguir.

## Redis

Em seu uso mais comum, quando utilizado como _cachestore_ ele é um substituto completo pra [memcached].
É capaz de prover performance similar ao anterior com a vantagem de também suportar a persistência em disco.

Isso é extremamente importante quando os servidores precisam ser reiniciados. Ao reiniciar um servidor efêmero,
como _memcached_, você perde todos os dados salvos na memória, o que causa durante um espaço de tempo, uma degradação da
performance na aplicação enquanto ela precisa gerar os dados que irão pro _cache_ novamente
(esse efeito é chamado _cold-cache_).

Redis por outro lado, ao preservar o _cache_, evita esse tipo de problema
(garantindo o que se chama de _hot-cache_, exatamente o oposto de _cold-cache_).

Redis também é muito utilizado em conjunto com o [Sidekiq], servindo como base de persistência para _background jobs_.
Esta funcionalidade é possível graças a uma série de estruturas de dados que permitem uma implementação nesse sentido.

Existe ainda algumas categorias de funcionalidades que são possíveis usando Redis e algumas de suas estruturas de dados
não convencionais:

* [Pub/Sub][redis-pubsub]: é um padrão onde um ou mais clientes publicam mensagens em canais e um ou mais clientes 
  escutam esses canais. O caso de uso mais comum é implementar um sistema de chat ou notificação em tempo real.
* [Message broker][redis-message-broker]: esta é geralmente a base de um sistema de _background jobs_, mas pode também
  servir para implementar event-sourcing (ou CQRS), uma técnica em que você armazena as mudanças como um conjunto
  sequencial de "eventos", que posteriormente é lido para obter o estado atual do sistema. Outro uso é
  como forma de implementar uma "transação" entre microserviços e serviços externos conhecido como [Saga].
* [Distributed locks][redis-locks]: por ser persistente e de alta performance, é possível manter travas temporárias
  (locks) distribuídas, que podem ser utilizados por partes do código para permitir que apenas um usuário ou
  processo execute aquele contexto de cada vez. O mais comum é utilizar isso em processos de _background jobs_, ou
  com microserviços.

### Instalando o Redis

No Ubuntu, ele está disponível no repositório padrão, portanto a instalação pode ser feita usando o
gerenciador de pacotes:

```bash
sudo apt install redis
```

O comando acima instala tanto o servidor quando o cliente em linha de comando (via pacote `redis-tools`).

Com o servidor instalado, você pode explorar os arquivos de configuração, no seguinte caminho:
`/var/lib/redis/redis.conf`, bem como se conectar usando o cliente de linha de comando nativo: `redis-cli`.

O Redis, por padrão aceita conexões apenas na sua máquina local e não exige nenhuma senha.

Se você quiser colocar uma senha, terá que editar o arquivo de configuração, modificando o seguinte trecho:

```bash
################################## SECURITY ###################################

# Require clients to issue AUTH <PASSWORD> before processing any other
# commands.  This might be useful in environments in which you do not trust
# others with access to the host running redis-server.
#
# This should stay commented out for backward compatibility and because most
# people do not need auth (e.g. they run their own servers).
#
# Warning: since Redis is pretty fast an outside user can try up to
# 150k passwords per second against a good box. This means that you should
# use a very strong password otherwise it will be very easy to break.
#
requirepass yourpasswordhere
```

E por fim, reiniciar o redis:

```bash
sudo systemctl restart redis.service
```

Em uma instalação de servidor, ao invés de usar uma senha simples, opte por usar uma ferramenta como `pwgen`, para
gerar uma senha segura, exemplo:

```bash
pwgen -n 80
```

Para se conectar ao banco via linha de comando, autenticando com a nova senha definida, basta digitar `redis-ctl`
como anteriormente usar o comando `auth yourpasswordhere` como primeiro comando. Se der tudo certo você verá isso:

```bash
127.0.0.1:6379> auth yourpasswordhere
OK
```

## MySQL e MariaDB

Antes de começar a falar em MySQL é preciso explicar um pouco da história desse banco de dados pra entender o papel
que ele teve no começo da internet, e a popularidade que ainda tem nos dias atuais.

O MySQL foi durante muitos anos o banco de dados de código aberto mais popular do mundo. A razão pra isso é bem
simples: existiam pouquíssimos bancos de dados open-source disponível, e ele ainda tinha por trás uma empresa de
grande porte na época (Sun Microsystems), o que garantia além de um grande nome, investimentos em marketing
(algo incomum para projetos open-source na época).

O fato de ser um projeto open-source de sucesso, fez com que ele passasse a vir disponível na maioria das
distribuições Linux, em seus repositórios principais. Pelo fato de não necessitar uma licença comercial,
era oferecido muitas vezes gratuitamente pelos provedores de hospedagem, como parte dos planos compartilhados
para rodar num pacote "LAMP" (Linux Apache MySQL e PHP).

Graças a essas duas combinações inúmeras aplicações de sucesso do início da internet foram construídas usando
principalmente essa _stack_. Dando destaque aqui para Wordpress como plataforma inicialmente de blogs e posteriormente
como CMS mais popular do mundo (ainda nos dias atuais).

Essa popularidade toda, fez com que MySQL se tornasse a solução padrão para novos projetos, principalmente open-source,
mesmo que não estivessem sendo construídos utilizando PHP.

Foi também o primeiro banco de dados que teve suporte oficialmente no Rails em suas primeiras versões
(já que era o banco de dados utilizado pelo projeto do Basecamp).

Como comentei anteriormente, a Sun Microsystems, não existe mais. A criadora original do MySQL e da linguagem Java,
foi comprada pela Oracle que passou a ter controle sobre ambos.

MySQL era disponibilizado em duas versões, a de código aberto e gratuita, também chamada de _Community Edition_,
e a comercial de código-fechado, Enterprise Edition. Apesar de praticamente não possuir nenhuma das características
avançadas e importantes do Oracle DB, o fato de ser um banco open-source e (portanto) gratuito, era um competidor
em muitos casos para Oracle.

Atualmente [existem 4 versões comerciais](https://www.mysql.com/products/), sendo que a mais barata começa em
2.000 USD/ano e a mais cara começa em 10.000 USD/ano (o preço varia conforme a quantidade de servidores,
quantos processadores existem na máquina, etc).

Com a aquisição do MySQL pela Oracle, e o fato de muita gente não acreditar que a Oracle continuaria mantendo a
ferramenta com o mesmo zelo e atenção, priorizando ou a sua versão comercial ou descontinuando oficialmente o mesmo,
um _fork_ surgiu. O MariaDB é um _fork_ iniciado por um dos desenvolvedores e criadores originais do MySQL.

Assim como a sua versão matriz, ele está disponível em versão open-source, gratuita como também oferece uma
solução comercial que inclui a versão open-source e uma série de plugins e ferramentas comercializadas a parte.

Existem diferenças razoáveis entre ambos os bancos, mas na maioria dos casos eles são intercambiáveis.
Existe inclusive formas fáceis de migrar entre um e outro, considerando uma lista pequena de incompatibilidades.

Como não poderia ser mais confuso que isso, você ainda pode encontrar menções a PerconaDB, Percona XtraDB ou
Percona Server for MySQL na internet, que é ainda uma terceira coisa diferente.

Eu explico: assim como MariaDB, Percona é também um _fork_ do MySQL. Diferente do MariaDB, o Percona promete 100%
de compatibilidade, e inclui apenas melhorias em cima da versão open-source atual do momento, utilizando geralmente
o sistema nativo de plugins e módulos, distribuindo por exemplo novas _storage engines_.

Entendendo essas diferenças todas vamos ver como instalar MySQL e posteriormente como instalar o MariaDB. O Percona
fica de fora, por que o maior foco dele são empresas que possuam um contrato de suporte do mesmo, mas como também é
open-source, nada impede você de começar a usá-lo.

### Instalando o MySQL

No Ubuntu, ele está disponível no repositório padrão, portanto a instalação pode ser feita usando o gerenciador 
de pacotes:

```bash
sudo apt install mysql-server mysql-client libmysqlclient-dev
```

Este comando instala o servidor, o cliente e dependências necessária para instalar a _gem_ de suporte ao MySQL no Ruby.
Ele não necessariamente instala a versão mais recente do banco de dados, mas a versão considerada mais
estável de quando sua versão do Ubuntu foi lançada.

Considerando as de suporte de longo termo (LTS), na 18.04 (lançada em abril de 2018) você encontra o MySQL 5.7,
enquanto na 20.04 (lançada em abril de 2020) você vai encontrar a 8.0.

Para fins de aprendizado, raramente é necessária uma versão específica. Em geral é preferível sempre utilizar a
versão mais nova disponível, por que você terá acesso a novas funcionalidades e melhorias de performance,
correções de bug etc.

Com o servidor instalado, você pode explorar os arquivos de configuração, no seguinte diretório:
`/etc/mysql/mysql.conf.d/`, bem como se conectar ao mesmo usando o cliente de linha de comando nativo: `mysql`.

É bastante recomendado que você também rode o seguinte script para configurar uma senha de administrador no banco:

```bash
sudo mysql_secure_installation
```

Após definir uma senha para usuário administrador (`root`), você deverá sempre usar a flag `-p` quando for usar o
comando do `mysql`, para que o mesmo solicite a senha. Exemplo:

```bash
mysql -u root -p
```

### Instalando o MariaDB

Semelhante ao procedimento de instalação do MySQL, o MariaDB também vem disponível no repositório padrão, e pode ser
instalado com o seguinte comando:

```bash
sudo apt install mariadb-server mariadb-client libmariadbd-dev
```

Detalhe importante: **não é possível manter o MySQL e o MariaDB instalados na mesma máquina**, você deve escolher um
deles.

MariaDB é um _fork_ do MySQL e o mesmo garante um alto nível de compatibilidade, inclusive sendo possível usar o mesmo
cliente para ambos os bancos. É possível em muitos casos, converter uma instalação rodando MySQL para MariaDB sem ser
necessário qualquer modificação nas configurações do banco ou da aplicação que depende dele, como ex. uma aplicação
Rails.

Algo que pode ser um pouco confuso é que ao instalar o MariaDB, apesar de ele também criar um comando `mariadb`, este na
verdade é apenas um link-simbólico (atalho) para o comando `mysql`. Todos os exemplos que comentei anteriormente no guia
de instalação do MySQL continuam valendo para o MariaDB.

Para configurar uma senha no banco com o usuário administrador (`root`), você usará o mesmo comando:
`sudo mysql_secure_installation`, assim como para se conectar usará `mysql -u root -p`. O comando `mysqladmin` também
está disponível e funciona exatamente da mesma forma que quando se tem o MySQL instalado.

## PostgreSQL

Este é o último banco de dados do nosso guia, e possivelmente a melhor opção considerando as alternativas equivalentes:
MySQL e MariaDB.

PostgreSQL é também um banco de dados relacional, como no caso anterior, e diferentemente do MySQL, não possui uma 
versão proprietária. Existem algumas organizações por trás do projeto, mas ele é primariamente mantido por NPOs 
(non-profit organizations), que à grosso modo pode ser compreendido como se fossem ONGs. Destaque principal para 
PgEU e PgUS, sendo as entidades jurídicas na União Europeia e Estados Unidos, respectivamente.

O fato do projeto ser mantido por NPOs garante que funcionalidades que teriam um apelo mais para grandes empresas, 
também sejam desenvolvidas e distribuídas livremente. Essa é uma das razões pelas quais é considerado o banco de dados 
de código-aberto mais avançado da atualidade.

Sua primeira versão foi lançado a mais de 30 anos atrás (em 1987), como um projeto patrocinado por varias agencias
governamentais americanas, entre elas a DARPA (a mesma responsável pela criação da Internet que usamos hoje), e durante 
a primeira década foi utilizado principalmente no meio acadêmico e em projetos de pesquisa, passando a ser adotado pela
indústria vários anos depois.

Se formos olhar para adoção do Postgres como banco de dados para aplicações web, só se tornou uma opção popular com o
surgimento de serviços de cloud, que tornou sua operação muito mais simples, como foi o caso do Heroku.

Naquela época dizia-se muito que Postgres era um banco avançado demais para usar para coisas simples como aplicações web.
Preferia-se bancos mais simples e (alegadamente mais rápidos) como MySQL para simplesmente construir "aplicações CRUD".

Daquela época para cá, todas as desconfianças quanto a capacidade dele em ambientes que exigiam performance ao invés de
recursos avançados, foi caindo por terra. Muitas aplicações hoje de escala global, utilizam PostgreSQL e conseguem ter
a disposição não só os recursos, como também velocidade e alta escalabilidade.

Explicado isso tudo, vamos a instalação.

### Instalando o PostgreSQL

Da mesma forma como as instalações anteriores, nós vamos usar os pacotes do sistema operacional para fazer a instalação:

```bash
sudo apt install postgresql postgresql-contrib
```

Se tudo ocorrer bem com a instalação, ao final dela é possível se conectar ao banco de dados apenas digitando:

```bash
sudo -u postgres psql

# para sair do client em linha de comando, basta digitar \q
```

Este acesso é possível e já seguro por padrão, graças a conexão acontecer usando _Unix sockets_, ao invés de uma conexão
de redes TCP/IP. Uma forma simples de explicar é imaginar que o servidor criou algo que parece com um arquivo, possuindo
todas as características de permissão inerentes a ele, mas na verdade é uma interface que permite entrada/saída.

Este é a forma ideal para comunicação cliente/servidor em uma mesma máquina, justamente por que já utilizar as formas
nativas do sistema operacional para cuidar de questões relacionadas a permissão de acesso, ao mesmo tempo que não o
torna acessível pela rede ou pela internet (note que é possível disponibilizar um _socket_ unix pela rede, mas isso 
não é recomendável e faz pouco sentido).

Caso queiramos utilizar esse servidor, da forma com que ele vem configurada, em uma aplicação Rails, no `database.yml`,
o que precisa ser configurado é o seguinte:

```yaml
development:
  adapter: postgresql
  encoding: unicode
  database: railsapp_development
  host: /var/run/postgresql # esta é a pasta onde o socket é criado
  port: 5432 # sockets não possuem portas, mas neste caso, ela é usada como parte do nome do socket (`.s.PGSQL.5432`)

test: &test
  adapter: postgresql
  encoding: unicode
  database: railsapp_test
  host: /var/run/postgresql
  port: 5432
```

Esta configuração vai utilizar como "usuário" para acessar a base de dados, o usuário que estiver rodando a aplicação.

Para criar esse acesso, você deve realizar algumas configurações a mais no servidor. No Postgres as configurações de
acesso são chamadas "_Roles_". Segue um exemplo de como atribuir a permissão para um usuário `rails` usando o
comando `createuser`:

```bash
sudo -u postgres createuser --interactive
Enter name of role to add: rails
Shall the new role be a superuser? (y/n) n
Shall the new role be allowed to create databases? (y/n) y
Shall the new role be allowed to create more new roles? (y/n) n
```

Da mesma forma, precisamos também criar as bases de dados que definimos no exemplo anterior:

```bash
sudo -u postgres createdb --owner=rails railsapp_development
sudo -u postgres createdb --owner=rails railsapp_test
```

Isso feito, supondo que você esteja no terminal utilizando o usuário `rails`, poderá acessar sua base de dados da
seguinte forma:

```bash
# base de desenvolvimento do rails:
psql railsapp_development

# base de testes do rails:
psql railsapp_test

# e lembrar que para sair basta digitar \q
```

Espero que esse guia seja suficiente para ajudar você a dar os primeiros passos. Tenho certeza que com essas instruções
vai ser possível seguir outros tutoriais e começar a testar na prática o que se está aprendendo.

Neste artigo utilizamos vários procedimento manuais com a finalidade de aprendizado, mas abordaremos em um artigo
futuro, algumas formas mais interessantes de rodar servidores, tanto para ambiente de desenvolvimento, quanto para
colocar aplicações em produção.

Se ficou alguma dúvida ou alguma instrução está incompleta, deixe nos comentários abaixo que vamos manter o guia sempre
atualizado.

[memcached]: https://www.memcached.org/
[Sidekiq]: https://sidekiq.org/
[redis-pubsub]: https://redis.io/topics/pubsub
[redis-message-broker]: https://redislabs.com/redis-enterprise/use-cases/messaging/
[redis-locks]: https://redis.io/topics/distlock
[Saga]: https://microservices.io/patterns/data/saga.html
