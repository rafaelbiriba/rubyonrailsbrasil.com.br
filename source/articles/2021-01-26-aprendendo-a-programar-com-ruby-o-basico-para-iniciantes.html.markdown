---

title: "Aprendendo a Programar com Ruby: O Básico para Iniciantes"
date: 2021-01-26 12:46 UTC
tags: iniciante, entrevistas
layout: post

---

Nos adicionamos na seção de [iniciantes](/iniciantes) um link para conteúdo produzido pelo
[Rafael Biriba](https://rafaelbiriba.com/about-me), focado em ensinar o básico de Ruby
através de video aulas gratuitas e em português.

Como forma de incentivar esse tipo de conteúdo, decidimos fazer uma mini-entrevista com ele
para que vocês possam conhecer profissionais da nossa área que estão em destaque fora do
país, e que continuam colaborando com a comunidade local.

**Conte-nos rapidamente como você iniciou a sua carreira e o que te trouxe para programação**

Nasci e fui e criado no Rio de Janeiro. Meu primeiro contato com computador por volta dos 6 anos de idade foi mágico.
Eu semprei fui muito curioso e já estava certo de que queria descobrir como os computadadores funcionavam.

Comecei a estudar por conta própria aos 11/12 anos com auxílio de livros e com o pouco da internet daquela época (anos 2000).

Aos 13 anos eu fazia um serviço de conversão de fitas VHS para DVD e criei um pequeno site para que meus clientes pudessem ver o
status dos pedidos. Foi meu primeiro contato com PHP e Mysql.

Aos 15 anos, eu já estava fazendo alguns sites de freelancer, e descobri que hospedar meus próprios sites eram bem mais baratos.
Contratei um servidor dedidado, e usando CPanel, abri um pequeno site de revendas de hospedagens.
Um ano depois disso, tive um pico de 15 clientes. Foi um periodo muito legal de aprendizagem.

Aos 18 anos, entrei na faculdade de análise de sistemas e também entrei no estágio da Globo.com antes de mesmo de começar o segundo período.

**Depois de entrar pra faculdade, e iniciar sua carreira, quais foram os desafios marcantes pelos quais você passou?**

Entre 2008 e 2016 eu trabalhei na área de vídeos da Globo.com. Tive experiências incríveis participando de projetos importantes como
transmissão da copa do mundo de 2010 e 2014, olimpíadas de inverno, e o famoso BBB. Nesse tempo eu fui mudando de times, e adquirindo mais
e mais experiências em projetos diferentes.

Em 2017, eu recebi uma proposta de emprego para trabalhar na Blacklane em Berlim (Alemanha) e resolvi aceitar esse desafio profissional e
também de vida, por conta da mudança de trabalho, país, idioma.

Na Blacklane (empresa de transporte com carros executivos) trabalhei com muitos sistemas internos, muitas APIs, dentre muitos outros
microserviços necessários para o ecosistema, como por exemplo receber alguns milhões de posições GPS de todos os motoristas.

Recentemente em 2020, eu participei e passei em um processo seletivo para Shopify. Até o momento da escrita dessa página, o meu
"primeiro dia" ainda não ocorreu, então não posso dar mais detalhes dessa nova fase! Mas estou ansioso para os próximos capítulos da vida.

**Para quem está entrando na área e queira se inspirar na sua tragetória, o que você recomendaria como estratégias que utiliza para
organizar o seu tempo ou estratégias de como melhorar ou maximiar a aprendizagem de uma nova linguagem**

Eu falei durante todo o meu curso que o segredo para ser bem sucedido na programação está na dedicação, na curiosidade e nos estudos.
Isso é bem importante, não só no início mas durante toda a carreira.

Desde muito tempo, eu tento organizar minha semana de maneira que consiga encaixar tudo o que eu gostaria e preciso fazer.
Eu utilizo uma ferramenta chamada todoist (um aplicativo de lista de tarefas), onde eu planejo e organizo minhas semanas com antecedencia.

Geralmente a organização é bem simples, eu procuro alternar lazer e estudo em meus horários livres. Dessa forma eu posso estudar algum
assunto num dia, ver um filme no outro, e assim sucessivamente. Funciona muito bem comigo, pois eu consigo equilibrar uma rotina
saudável de estudo e diversão.

**O que te motivou a começar a ensinar e a criar um curso de programação?**

A idéia surgiu em 2019, quando eu estava lendo as notícias do Brasil, e notei que a economia do Brasil só piorava.
Ao mesmo tempo, incontáveis empresas no mundo todo estão contratando desenvolvedores com bons salários.

Ainda nesse ritmo de reflexão, eu me questionava sobre como fazer bom uso dos meus +11 anos de experiência profisional. Foi assim que
surgiu a idéia de tentar fazer um curso para iniciantes.

A proposta é beneficiar pessoas que nunca tiveram contato com a programação e, quem sabe, apresentar uma oportunidade de mudança de
carreira. O planejamento das aulas durou meses. Considerando um público iniciante, foi um desafio traduzir os conceitos e conteúdos para
uma linguagem mais simples para que os vídeos não se tornassem longos e complexos.

O objetivo principal é ensinar o básico do básico, para que a pessoa siga estudando por conta própria.
Ao todo são 18 aulas, a maioria dividida em parte A e B, ou seja, 34 vídeo aulas. Dessa forma, eu busco alcançar pessoas que
imaginavam que programação é díficil.

A idéia é abrir portas e mostrar caminhos para esse mundo mágico onde códigos se transformam em incríveis sistemas.

**Como as pessoas podem te ajudar ou agradecer por ter dedicado esse seforço em contruir um conteúdo de qualidade e disponibilizá-lo
gratuitamente para a comunidade?**

Como disse em um dos meus vídeos que ao final do curso, se eu conseguisse influenciar ou ajudar pelo menos uma pessoa,
eu já estaria muito feliz.

A melhor forma de você me agradecer hoje é se increver no [meu canal no Youtube](https://www.youtube.com/c/RafaelBiriba),
curtir os vídeos, e o principal, deixar algum tipo de comentário ou mesmo um feedback de como eu posso fazer vídeos melhores no futuro.

É bastante gratificante ter essa interação.

Fazer as vídeo aulas tomaram muito mais tempo que eu poderia ter imaginado. Para disponibilizar 1h de aula, eu gasto no mínimo outras
8h para preparar o conteúdo, escrever no blog, testar os exercícios, gravar, editar e publicar. Se você assistiu o curso, vai assistir
ou apenas apreciou o meu trabalho, você [pode me pagar um café](http://bit.ly/obrigado_rafaelbiriba).

Mas, honestamente, eu quero mesmo é ajudar. Um simples "muito obrigado, gostei bastante" nos comentários do YouTube,
já me deixa feliz pelo dia inteiro.
